# README #

[LAP.json](./Lap.json) derives from [AWS /cloudformation-templates-us-east-1/LAMP_Single_Instance.template](https://s3-external-1.amazonaws.com/cloudformation-templates-us-east-1/LAMP_Single_Instance.template). It removed mysql installation and database creation and added deployment of PHP code from a git repository. 

The code repository from where to pull the application code and commit are specified at launch time.  

Monika Mevenkamp developed this for her 5 Min Lightning talk at Princeton University's IT Unconference 

Slides: http://itunconference.princeton.edu/wp-content/uploads/sites/366/2015/03/4-Mevenkamp-Infrastructure.pptx

