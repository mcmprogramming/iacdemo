<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Deployed with AWS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">
</head>
<body>
<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="inner cover">

                <h1>Here Fresh from AWS </h1>
                <h3>
                    <?php
                    // Setup a handle for CURL
                    $curl_handle = curl_init();
                    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

                    // Get the instance-id of the intance from the instance metadata
                    curl_setopt($curl_handle, CURLOPT_URL, 'http://169.254.169.254/latest/meta-data/instance-id');
                    $instanceid = curl_exec($curl_handle);
                    if (empty($instanceid)) {
                        print "Sorry, for some reason, we got no instance id back <br/>";
                    } else {
                        print "EC2 instance-id <br/>" . $instanceid . "<br/>";
                    }
                    ?>

                </h3>

                <h3>
                    <a class="btn btn-default" href="./LAP.json"> deployed with AWS template </a>
                </h3>

                <h3>
                    <a class="btn btn-default" href="./info.php"> PHP info </a>
                </h3>

            </div>
        </div>
    </div>

</body>
</html>
