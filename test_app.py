import urllib.request
import sys
import boto3
import argparse


def main():
    args = _parseargs()

    cloudformation = boto3.resource('cloudformation')
    stack = cloudformation.Stack(args.stack_name)
    url = [v['OutputValue'] for v in stack.outputs if v['OutputKey'] == 'WebsiteURL'][0]

    print("testing " + url)
    try:
        urllib.request.urlopen(url)
        print("passed")
    except:
        print("failed")
        sys.exit(1)


def _parseargs():
    description = """given the stack-name test the WebSiteURL of the stack
for actual applications this should include trigering test case execution via remote login 
"""
    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--stack-name", "-s", default=None, required=True, help="cloud formation stack name")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()


