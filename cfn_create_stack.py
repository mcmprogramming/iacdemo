# Copyright 2017 Princeton University
"""
CLI utility to create AWS stack from cloud formation template 

run with --help option for details 
"""
import boto3
from botocore.exceptions import ClientError, WaiterError

import os
import sys
import argparse
import logging


def _create_stack(client, arguments):
    """
    create cloudformation stack with the supplied template and parameters
    """
    logging.debug("_create_stack:arguments " + str(arguments))
    try:
        client.create_stack(
            StackName=arguments.stack_name,
            TemplateBody=open(arguments.template, 'r').read(),
            Capabilities=['CAPABILITY_IAM'],
            Parameters= [{'ParameterKey': 'InstanceType', 'ParameterValue': arguments.instance_type},
                         {'ParameterKey': 'KeyName', 'ParameterValue': arguments.key_name},
                         {'ParameterKey': 'GitRepo', 'ParameterValue': arguments.git_repo},
                         {'ParameterKey': 'GitCommit', 'ParameterValue': arguments.git_commit}]
        )
        logging.info("waiting ... ")
        waiter = client.get_waiter('stack_create_complete')
        waiter.wait(StackName=arguments.stack_name)
        return True
    except ClientError as err:
        print("Failed to create the stack.\n" + str(err))
    except IOError as err:
        print("Failed to access file \n\t" + str(err))
    except WaiterError as err:
        print("The Stack Creation did not successfully complete.  " \
              "Check CloudFormation events for more information. \n" + str(err))
    return False


class ArgParser(argparse.ArgumentParser):
    LOGLEVELS = ['CRITICAL', 'ERROR', 'WARN', 'INFO', 'DEBUG', 'NOTSET']

    def missing_env_argument(self, arg_name, env_var_name):
        self.error("must give %s parameter or set %s environment variable" % (arg_name, env_var_name))

    def parse_args(self):
        args = argparse.ArgumentParser.parse_args(self)
        if (not (args.instance_type)):
            self.missing_env_argument("--instance_type", "AWS_INSTANCE_TYPE")
        if (not (args.key_name)):
            self.missing_env_argument("--key_name", "AWS_KEY_NAME")
        return args


    def add_env_argument(self, arg_name, env_var_name, help_str):
        try:
            env_value = os.environ[env_var_name]
            help_str += " - default " + env_value
        except:
            env_value = None
        self.add_argument(arg_name, required=False,
                          default=env_value,
                          help=help_str)


    def add_argunments(self):
        self.add_argument("--stack-name", "-s", required=True, default=None, help="cloud formation stack name")
        self.add_env_argument("--template", "AWS_CFN_TEMPLATE", "cloudformation stack template file")
        self.add_env_argument("--key-name", 'AWS_KEY_NAME', 'aws key name')
        self.add_env_argument("--instance_type", 'AWS_INSTANCE_TYPE', 'aws instance type')
        self.add_env_argument("--git-repo", 'GIT_REPO', 'git repository url (public)')
        self.add_env_argument("--git-commit", 'GIT_COMMIT', 'git commit hash or branch name')
        self.add_argument("--loglevel", choices=ArgParser.LOGLEVELS,
                            default=logging.ERROR,
                            help="log level  - default ERROR")

def main():

    description = """
Create a cloud format stack based on the given template. 
The stack is parameterized based on the given arguments and/or corresponding environment variables
"""
    parser = ArgParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argunments()
    args = parser.parse_args()

    logging.getLogger().setLevel(args.loglevel)
    logging.basicConfig()
    try:
        client = boto3.client('cloudformation')
    except ClientError as err:
        print("Failed to create boto3 cloudformation client.\n" + str(err))
        return False

    stack_name = args.stack_name
    logging.info("stack-name: %s  template: %s" % (stack_name, args.template))
    if not _create_stack(client, args):
        sys.exit(1)

    print('---', file=sys.stderr)
    print("--- Created Stack: %s" % (args.stack_name), file=sys.stderr)
    print('---', file=sys.stderr)

if __name__ == "__main__":
    main()
